#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io       import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(55, 55)
        self.assertEqual(v, 113)

    def test_eval_6(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_7(self):
        v = collatz_eval(1234, 4321)
        self.assertEqual(v, 238)

    def test_eval_8(self):
        v = collatz_eval(837799, 837799)
        self.assertEqual(v, 525)

    def test_eval_9(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_10(self):
        v = collatz_eval(21474, 83647)
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n55 55\n10 1\n1234 4321\n837799 837799\n1 1\n21474 83647\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n55 55 113\n10 1 20\n1234 4321 238\n837799 837799 525\n1 1 1\n21474 83647 351\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
